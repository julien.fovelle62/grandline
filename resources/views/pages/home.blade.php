@section('content')
@extends('index')

<section class="home">
  <div class="mdc-layout-grid">
    <div class="mdc-layout-grid__inner">

      <!-- CARTE MATERIAL ARTICLE -->
      <div class="mdc-layout-grid__cell--span-4-desktop mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-6-phone">
        <a href="/article">
        @include('components.cards')
        </a>
      </div>
      <!---->

    </div>
  </div>
</section>

@stop


