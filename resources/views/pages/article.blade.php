@section('content')
@extends('index')

<section class="article-container">

    <header class="article-head">
        <div class="mdc-layout-grid">
            <div class="mdc-layout-grid__inner">
                <div
                    class="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-6-phone">
                    <div class="article-padding">
                        <!-- TITRE -->
                        <h1 class="article-head__title mdc-typography--headline3">Immigration Courts Are Relying on Bad
                            Tech</h1>
                        <!-- SOUS TITRE -->
                        <h2 class="article-head__subtitle mdc-typography--headline5">Immigration courts increasingly
                            rely on video teleconference
                            hearings, but those proceedings come with their own problems</h2>
                        <!-- PHOTO -->
                        <img class="article-head__photo" src="{{ asset('img/article.jpg') }}" alt="">
                        <!-- CREDITS -->
                        <div class="article-head__credits">
                            <span>Credits: Erik McGregor/Pacific Press/LightRocket via Getty</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <article class="article-content">
        <div class="mdc-layout-grid">
            <div class="mdc-layout-grid__inner">
                <div
                    class="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-6-phone">
                    <div class="article-content__content">
                        <!-- CONTENU DE L'ARTICLE -->
                        <p class="article-padding">The Department of Justice claims that such VTC hearings save money
                            and time. While the increased
                            efficiency is in dispute by attorneys and advocates, it does seem to be resulting in more
                            deportations.
                            But immigration attorneys claim that VTC hearings undercut their clients’ access to due
                            process and
                            contribute to the growing backlog of pending immigration cases. Given the ever-expanding
                            role such a
                            process plays in immigration courts — in 2018 there were more than 125,000 TVC hearings, a
                            14.5%
                            increase from the previous year — it’s quite possible that, in the near future, people
                            coming to this
                            country seeking asylum will never see a judge in person.</p><br>
                        <p class="article-padding">The idea behind VTC is that remote hearings will make the
                            overburdened process run more efficiently. But
                            technological glitches, faulty equipment, untrained or unaccustomed users, as well as
                            increased
                            difficulty for judges in reading the demeanor of a claimant or for attorneys to have
                            confidential face
                            time with their clients, all add up to what some activists claim is actually exacerbating
                            the swelling
                            backlog of cases — currently approaching 900,000. A 2017 study from the Executive Office of
                            Immigration
                            Review (EOIR) found that “issues with poor video and sound quality, can disrupt cases to the
                            point that
                            due process issues may arise.”</p><br>
                        <div class="article-content__photo article-padding">
                            <img src="{{ asset('img/article.jpg') }}" alt="">
                        </div>
                        <p class="article-padding">The idea behind VTC is that remote hearings will make the
                            overburdened process run more efficiently. But
                            technological glitches, faulty equipment, untrained or unaccustomed users, as well as
                            increased
                            difficulty for judges in reading the demeanor of a claimant or for attorneys to have
                            confidential face
                            time with their clients, all add up to what some activists claim is actually exacerbating
                            the swelling
                            backlog of cases — currently approaching 900,000. A 2017 study from the Executive Office of
                            Immigration
                            Review (EOIR) found that “issues with poor video and sound quality, can disrupt cases to the
                            point that
                            due process issues may arise.”</p><br>
                    </div>
                </div>
            </div>
        </div>
    </article>
    
</section>

@include('components.suggestions')

@stop
