<head>
    <html lang="{{app()->getLocale()}}">
    <meta charset="utf-8">
    <title>GrandLine — the booklet in your pocket.</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Medias — Create, write and discovers articles.">
    <meta name="author" content="Fovelle Julien">
    <meta name="keywords" content="Blog, article, medias, write">
    <!--<link rel="icon" href="src/img/logo/logo-32.png" type="image/x-icon"/>-->

    <!--Parameters-->
    <meta name="referrer" content="no-referrer-when-downgrade" />
    <meta name="robots" content="all" />
    <link rel="manifest" href="/manifest.json">

    <!-- StyleSheets -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">


    <!-- Meta SocialMedia -->
    <meta property="og:site_name" content="GrandLine.">
    <meta property="og:locale" content="fr_FR">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="GrandLine — the booklet in your pocket." />
    <meta property="og:description" content="The booklet in your pocket.">
    <meta property="og:url" content="https://grandline.fr/" />
    <meta property="og:image" content="src/img/..." />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{csrf_token()}}">
</head>

<body>
    <!-- HEADER -->
    @include('main.header')
    <!-- CONTENT -->
    @yield('content')

</body>

<!-- Script files -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
