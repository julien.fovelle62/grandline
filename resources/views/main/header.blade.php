<!--==== HEADER ====-->
<header class="header">
    <div class="mdc-layout-grid">
        <div class="mdc-layout-grid__inner">
            <div
                class="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-6-phone">
                <div class="header-menu">
                    <div class="header-menu__logo">
                        <a href="/">
                        <img src="{{ asset('img/Grandline.svg') }}" alt="medias-logo">
                        </a>
                    </div>
                    <div class="header-menu__nav">
                        <ul class="header-menu__nav-list">
                            <li><a class="mdc-button"><i class="material-icons search-icon">search</i></a></li>
                            <li><a class="mdc-button"><i class="material-icons-outlined">bookmarks</i></a></li>
                            <li><a class="mdc-button"><i class="material-icons-outlined">notifications</i></a></li>
                            <li>
                                <a class="nav__icon-profile">
                                    <div class="profile-head"> </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!--==== SUBHEADER ====-->
<section class="subheader" id="wrap">
    <div class="mdc-layout-grid">
        <div class="mdc-layout-grid__inner">
            <div class="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-6-phone">
                <!-- Navigation -->
                <div class="subheader__category">
                    <ul>
                        <li><a class="active" href="/">HOME</a></li>
                        <li><a href="">CATEGORIE 1</a></li>
                        <li><a href="">CATEGORIE 2</a></li>
                        <li><a href="">CATEGORIE 3</a></li>
                        <li><a href="">CATEGORIE 4</a></li>
                        <li><a href="">CATEGORIE 5</a></li>
                        <li><a href="">CATEGORIE 6</a></li>
                        <li><a href="">CATEGORIE 7</a></li>
                        <li><a href="">CATEGORIE 8</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
