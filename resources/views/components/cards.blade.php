<section class="article-card">
    <div class="mdc-ripple-surface--primary mdc-card ">

        <div class="mdc-card__primary-action card__primary-action" tabindex="0">
            <div class="mdc-card__media mdc-card__media--16-9 card__media"
                style="background-image: url(&quot;https://loremflickr.com/320/240/paris,girl/all&quot;);">
                <div class="mdc-card__media-content card__media-content">
                    <div class="card__primary">
                        <!-- TITRE -->
                        <h2 class="card__title mdc-typography mdc-typography--headline6">Titre de l'article</h2>
                        <!-- AUTEUR -->
                        <h3 class="card__subtitle mdc-typography mdc-typography--subtitle2">par Julien Fovelle</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- SOUS TITRE -->
        <div class="card__secondary mdc-typography mdc-typography--body2">Visit ten places on our planet that are
            undergoing the biggest changes today.</div>

        <div class="mdc-card__actions card__actions">
            <div class="mdc-card__action-buttons card__actions-buttons">
                <button class="mdc-button mdc-card__action mdc-card__action--button">Lire</button>
            </div>
            <div class="mdc-card__action-icons card__actions-icons">
                <button id="add-to-favorites" class="mdc-icon-button" aria-label="Add to favorites" aria-hidden="true" aria-pressed="false">
                    <i class="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">favorite</i>
                    <i class="material-icons mdc-icon-button__icon">favorite_border</i>
                </button>
            </div>
        </div>

    </div>
</section>
