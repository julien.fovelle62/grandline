import {MDCRipple} from '@material/ripple';

const iconButtonRipple = new MDCRipple(document.querySelector('.mdc-icon-button'));
iconButtonRipple.unbounded = true;


//CARDS RIPPLE 
new MDCRipple(document.querySelector('.mdc-card'));

//-- SCROLL SUBHEADER

let prevScrollpos = window.pageYOffset;

if ($('.subheader')[0]) {
    window.onscroll = function () {
        var currentScrollPos = window.pageYOffset;
        if ($(this).scrollTop() >= 50) {
            console.log('test');
                document.querySelector(".subheader").style.position = "fixed";
            } else {
                document.querySelector(".subheader").style.position = "static";
            }
        prevScrollpos = currentScrollPos;
    };
}