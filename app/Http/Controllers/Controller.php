<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    public function home(){
        //Ici ce trouve le controller pour la page d'accueil

        return view('pages.home');
        
        }
        
    public function article(){
        //Ici ce trouve le controller pour la page articles

        return view('pages.article');
        
    }
}
