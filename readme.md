# GrandLine - Tutoriel Voyager

Le site est développé en utilisant le framework PHP **Laravel** ainsi que sa dépendance **Voyager** servant de CMS au projet. Pour pouvoir commencer à développer aussi bien le *front-end* que le *back-end* du site il y a quelques choses à faires.

## Installation

Premièrement, cloner le dépot [Gitlab](https://gitlab.com/kosnos/grandline).
Ensuite déplacer vous dans le dépot
```bash
$ cd grandline
```
Creer la base de donnee dans phpMyAdmin

Installer les dépendances de *Composer*
```bash
$ composer install
```
Installer les dépendances de *Node*

```bash
$ npm install
```
Ensuite il reste à configurer les variables d'environnement, pour créer le fichier faites ceci.
```bash
$ cp .env.example .env
```

Et pour générer une nouvelle clé
```bash
$ php artisan key:generate
```

Puis il faut qu'au sein de ce fichier `.env` vous renseigner les différents informations concernant votre base de données c'est à dire

`DB_HOST` 
`DB_DATABASE`
`DB_USERNAME`
`DB_PASSWORD`

 Lier la base de donnée portant le nom renseigner dans `DB_DATABASE`.

Plus qu'une commande :
```bash
$ php artisan voyager:install
```

Voilà !

## Compilation

 Et mais il manque les styles, pour les compiler un petit coup de 
```bash
$ npm run dev
``` 

## Dépendences

### SCSS 
Pour ajouter du code scss, il faut créer le fichier au bon endroit dans ressources/scss

Il faut créer ensuite l'importer dans le fichier **common.scss**


### JS 
Pour installer les dépendences, il faut passer par les modules npm.
Une fois votre module installé entièrement par le bais de la commande

```bash
$ npm install * --save
``` 

Il faut créer une lib dans la section /ressources/lib et l'importer dans le fichier **bootloader.js**

